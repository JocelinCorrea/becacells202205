class Queue {
    constructor (){
        const list = [];

        this.enqueue = function (type){
            list.push(type);
            return type;
        };
        this.dequeue = function(){
            return list.shift();
        };
    }
}

var q = new Queue;

q.enqueue(9);
q.enqueue(8);
q.enqueue(7);

console.log(q.dequeue()); //imprime 9 ya que es el primer elemento despues lo elimina
console.log(q.dequeue()); //imprime 8 ya que ahora es el primer elemento despues lo elimina
console.log(q.dequeue()); //imprime 7 ya que ahora es el primer elemento despues lo elimina
console.log(q); //regresa el Objeto el primer elemento estará vacío el segundo será el tipo
console.log(Object.keys(q)); //imprimira las llaves del objeto "enqueue" y "dequeue"

