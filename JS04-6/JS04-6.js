const classInstance = new class {
    get prop (){
        return 5;
    }
};

classInstance.prop = 10;
console.log(classInstance.prop);

//El resultado mostrado en consola sera el número 5 
//debido a que es el return que se tiene en la clase
//independientemente del valor que se le asigne 
//regresará un 5