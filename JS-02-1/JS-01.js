
const clientes = ['Luis', 'Omar', 'Maria', 'Carmen'];
document.getElementById('clientes').innerHTML = clientes
const empleados = ['Iker','Guadalupe','Lucia','Ethan'];
document.getElementById('empleados').innerHTML = empleados

//método CONCAT
const concat = clientes.concat(empleados);
document.getElementById('concat').innerHTML = concat

//método JOIN
const join = clientes.join(empleados);
document.getElementById('join').innerHTML = join

//método PUSH
const push = clientes.push(empleados);
document.getElementById('push').innerHTML = clientes

//método SPLICE
const splice = clientes.splice(1, 1, empleados);
document.getElementById('splice').innerHTML = clientes