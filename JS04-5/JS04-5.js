class MyClass {
    constructor (){
        this.names_ = [];
    }

    set name (value){
        this.names_.push(value);
    }

    get name () {
        return this.names_[this.names_.length-1];
    }
}

const myClassInstance = new MyClass();
myClassInstance.name = 'Joe';
myClassInstance.name = 'Bob';

console.log(myClassInstance.name);
//Imprime el ultimo valor asignado a name en este caso sería Bob
console.log(myClassInstance.names_);
//Imprime el arreglo que se ha generado al agregar los nombres 0:"Joe", 1:"Bob"