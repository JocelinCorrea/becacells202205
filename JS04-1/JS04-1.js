var anObject = {
    foo: 'bar',
    length: 'interesting',
    '0':'zero!',
    '1':'one!'
};

var anArray = ['zero.', 'one.'];

console.log (anArray[0],anObject[0]);
console.log (anArray[1],anObject[1]);
console.log (anArray.length,anObject.length);
console.log (anArray.foo,anObject.foo);

console.log(typeof anArray == 'object', typeof anObject == 'object');
console.log(anArray instanceof Object, anObject instanceof Object);
console.log(anArray instanceof Array, anObject instanceof Array);
console.log(Array.isArray(anArray),Array.isArray(anObject));